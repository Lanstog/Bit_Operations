#include "shift.h"
int shift(int a, int b)
{
	for (int i = 0; i < b; i++) { a >>= 1; };
	return a;
}