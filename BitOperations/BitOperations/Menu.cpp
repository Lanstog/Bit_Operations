#include "Proizvedenie.h"
#include "Summa.h"
#include "Is_Natural.h"
#include "XOR.h"
#include "Degree.h"
#include "iostream"
#include "negation.h"
#include "shift.h"
#include "shift1.h"
#include "string"
#include "Check.h"
#include "Lights.h"
using namespace std;
void menu()
{
	int a = 0, b = 0, i = 0;
	string vvod;
	while (i != 9)
	{
		i = 0;
		system("cls");
		cout << "�������� �������:\n1)��������� ���������\n2)��������� ��������\n3)��������� \"����������� ���\"\n4)��������� ����� ������\n5)��������� ����� �����\n6)��������� ���������\n7)�������� �� �������\n8)�������� �������\n9)���������� ���������" << endl;
		getline(cin, vvod);
		if (!check(vvod))
		{
			i = -1;
		}
		else
			i = atoi(vvod.c_str());
		system("cls");
		switch (i)
		{
		case 1:cout << "������� ����� a: ";
			getline(cin, vvod);
			if (!check(vvod))
			{
				a = -1;
			}
			else
				a = atoi(vvod.c_str());
			cout << "������� ����� b: ";
			getline(cin, vvod);
			if (!check(vvod))
			{
				b = -1;
			}
			else
				b = atoi(vvod.c_str());
			if (is_natural(a) && is_natural(b))
				cout << "��������� ������������ ����� " << a << " � ����� " << b << " �����: " << proiz(a, b) << endl;
			else
			{
				cout << "������! ���� �� ����� �� �������� �����������!" << endl;
			}
			break;
		case 2:cout << "������� ����� a: ";
			getline(cin, vvod);
			if (!check(vvod))
			{
				a = -1;
			}
			else
				a = atoi(vvod.c_str());
			cout << "������� ����� b: ";
			getline(cin, vvod);
			if (!check(vvod))
			{
				b = -1;
			}
			else
				b = atoi(vvod.c_str());
			if (is_natural(a) && is_natural(b))
				cout << "��������� ����� ����� " << a << " � ����� " << b << " �����: " << summa(a, b) << endl;
			else
			{
				cout << "������! ���� �� ����� �� �������� �����������!" << endl;
			}
			break;
		case 3:
			cout << "������� ����� a: ";
			getline(cin, vvod);
			if (!check(vvod))
			{
				a = -1;
			}
			else
				a = atoi(vvod.c_str());
			cout << "������� ����� b: ";
			getline(cin, vvod);
			if (!check(vvod))
			{
				b = -1;
			}
			else
				b = atoi(vvod.c_str());
			if (is_natural(a) && is_natural(b))
				cout << "����������� ��� ����� " << a << " � " << b << " ����� " << xor (a, b) << endl;
			else
			{
				cout << "������! ���� �� ����� �� �������� �����������!" << endl;
			}
			break;
		case 4:cout << "������� �����: ";
			getline(cin, vvod);
			if (!check(vvod))
			{
				a = -1;
			}
			else
				a = atoi(vvod.c_str());
			cout << "������� ����� �������: ";
			getline(cin, vvod);
			if (!check(vvod))
			{
				b = -1;
			}
			else
				b = atoi(vvod.c_str());
			if (is_natural(a) && is_natural(b))
				cout << "��������� ����� ����� " << a << " �� " << b << " ������ �����: " << shift(a, b) << endl;
			break;
		case 5:cout << "������� �����: ";
			getline(cin, vvod);
			if (!check(vvod))
			{
				a = -1;
			}
			else
				a = atoi(vvod.c_str());
			cout << "������� ����� �������: ";
			getline(cin, vvod);
			if (!check(vvod))
			{
				b = -1;
			}
			else
				b = atoi(vvod.c_str());
			if (is_natural(a) && is_natural(b))
				cout << "��������� ����� ����� " << a << " �� " << b << " ������ �����: " << shift1(a, b) << endl;
			break;
		case 6:cout << "������� �����: ";
			getline(cin, vvod);
			if (!check(vvod))
			{
				a = -1;
			}
			else
				a = atoi(vvod.c_str());
			if (is_natural(a) && is_natural(b))
				cout << "��������� ��������� ����� " << a << " �����: " << negg(a) << endl;
			break;
		case 7:
			cout << "������� �����: ";
			getline(cin, vvod);
			if (!check(vvod))
			{
				a = -1;
			}
			else
				a = atoi(vvod.c_str());
			if (is_natural(a))
			{
				cout << "����� " << a << " ";
				if (degree(a) == true)
					cout << "�������� ";
				else cout << "�� �������� ";
				cout << "�������� ������" << endl;
			}
			else
			{
				cout << "������! ���� �� ����� �� �������� �����������!" << endl;
			}
			break;
		case 8:
			lights();
			break;
		case 9:break;
		default:system("cls");
			cout << "������� ���������� �������!" << endl;
			break;
		}
		if (i == 8)
			cout << "���������� ���������..." << endl;
		system("pause");
		system("cls");
	}
}