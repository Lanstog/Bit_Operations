#include "shift1.h"
int shift1(int a, int b)
{
	for (int i = 0; i < b; i++) { a <<= 1; };
	return a;
}