#include "stdafx.h"
#include "CppUnitTest.h"
#include "../BitOperations/Proizvedenie.cpp"
#include "../BitOperations/Is_Natural.cpp"
#include "../BitOperations/Summa.cpp"
#include "../BitOperations/shift.cpp"
#include "../BitOperations/shift1.cpp"
#include "../BitOperations/negation.cpp"
#include "../BitOperations/Degree.cpp"
#include "../BitOperations/XOR.cpp"
#include "../BitOperations/Check.cpp"


using namespace Microsoft::VisualStudio::CppUnitTestFramework;

namespace BitOperations_Test
{
	TEST_CLASS(UnitTest1)
	{
	public:
		TEST_METHOD(Proizvedenie_is_0)
		{
			int a = 1;
			int b = 1;
			Assert::AreNotEqual(0, proiz(a, b));
		}
		TEST_METHOD(Proizvedenie_is_1)
		{
			int a = 1;
			int b = 1;
			Assert::AreEqual(1, proiz(a, b));
		}
		TEST_METHOD(Is_not_Natural)
		{
			int a = -1;
			Assert::AreEqual(false, is_natural(a));
		}
		TEST_METHOD(Is_Natural)
		{
			int a = 1;
			Assert::AreEqual(true, is_natural(a));
		}
		TEST_METHOD(Sum_is_1)
		{
			int a = 1, b = 1;
			Assert::AreEqual(1, summa(a, b));
		}
		TEST_METHOD(Negation_is_done)
		{
			int a = 2;
			Assert::AreEqual(5, negg(a));
		}
		TEST_METHOD(Shift_is_done)
		{
			int a = 4, b = 2;
			Assert::AreEqual(1, shift(a, b));
		}
		TEST_METHOD(Left_shift_is_done)
		{
			int a = 4, b = 2;
			Assert::AreEqual(16, shift1(a, b));
		}
		TEST_METHOD(Degree_is_true)
		{
			int a = 8;
			Assert::AreEqual(true, degree(a));
		}
		TEST_METHOD(Degree_is_false)
		{
			int a = 11;
			Assert::AreEqual(false, degree(a));
		}
		TEST_METHOD(XOR_is_done)
		{
			int a = 5, b = 9;
			Assert::AreEqual(12, xor (a, b));
		}
		TEST_METHOD(Check_is_true)
		{
			string chec = "1";
			Assert::AreEqual(true, check(chec));
		}
		TEST_METHOD(Check_is_false)
		{
			string chec = "Woop Woop";
			Assert::AreEqual(false, check(chec));
		}		
	};
}