![alt Link](https://i.ytimg.com/vi/CU-0d25fJtk/hqdefault.jpg)


# Краткое описание программы
В данном проекте представлена программа на языке c++, связанная с выполнением 
различных битовых операций. 

Для более детального изучения перейдите 
по [ссылке](https://gitlab.com/Lanstog/Bit_Operations/wikis/home).